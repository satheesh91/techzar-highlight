@php
    $user = Auth::user();
    $currentUrl = url()->current();
@endphp
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>{{ config('app.name', 'Project Management') }} @isset($title) :: {{ $title }} @endisset</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="{{ asset('common/img/logo/favicon.png') }}" type="image/x-icon"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Fonts and icons -->
    <script src="{{ asset('private/assets/js/plugin/webfont/webfont.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {"families":["Lato:300,400,700,900"]},
            custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset('private/assets/css/fonts.min.css') }}']},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{ asset('private/assets/css/bootstrap.min.css') }}"> 
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="{{ asset('private/assets/css/demo.css') }}">
    
    <!-- Datatable CSS -->
    <link rel="stylesheet" href="{{ asset('common/vendor/datatable/datatables.min.css') }}">

    <!-- X Editable CSS -->
    <link rel="stylesheet" href="{{ asset('common/vendor/x-editable/css/bootstrap-editable.css') }}">

    <!-- Select 2 CSS -->
    <link rel="stylesheet" href="{{ asset('common/vendor/select2/css/select2.min.css') }}">

    <!-- Date Range Picker CSS -->
    <link rel="stylesheet" href="{{ asset('common/vendor/daterangepicker/daterangepicker.css') }}">

    <!-- Image Picker CSS -->
    <link rel="stylesheet" href="{{ asset('common/vendor/fileinput/css/jasny-bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('private/assets/css/atlantis.min.css') }}">

    <link rel="stylesheet" href="{{ asset('common/css/style.css') }}">

    <link rel="stylesheet" href="{{ asset('common/css/user.css') }}">
   
</head>
<body>
    <div class="wrapper">
        <div class="main-header">
            <!-- Logo Header -->
            <div class="logo-header" data-background-color="blue">
                
                <a href="{{ route('users') }}" class="logo text-center text-white">
                    <!-- <img src="{{ asset('common/img/logo/logo-small.png') }}" alt="navbar brand" class="navbar-brand w-50"> -->
                    Highlight
                </a>
                <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                        <i class="icon-menu"></i>
                    </span>
                </button>
                <button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
                <div class="nav-toggle">
                    <button class="btn btn-toggle toggle-sidebar">
                        <i class="icon-menu"></i>
                    </button>
                </div>
            </div>
            <!-- End Logo Header -->

            <!-- Navbar Header -->
            <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">
                
                <div class="container-fluid">
                    <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                        <li class="nav-item toggle-nav-search hidden-caret">
                            <a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
                                <i class="fa fa-search"></i>
                            </a>
                        </li>
                        
                        <li class="nav-item dropdown hidden-caret">
                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                                <div class="avatar-sm">
                                    <img src="{{ empty($user->profile_pic) ? avatar($user->name) : asset($user->profile_pic) }}" alt="..." class="avatar-img rounded-circle">
                                </div>
                            </a>
                            <ul class="dropdown-menu dropdown-user animated fadeIn">
                                <div class="dropdown-user-scroll scrollbar-outer">
                                    <li>
                                        <div class="user-box">
                                            <div class="avatar-lg"><img src="{{ empty($user->profile_pic) ? avatar($user->name) : asset($user->profile_pic) }}" alt="image profile" class="avatar-img rounded"></div>
                                            <div class="u-text">
                                                <h4>{{ $user->name }}</h4>
                                                <p class="text-muted">{{ $user->email }}</p>
                                                <a href="{{ route('user.profile') }}" class="btn btn-xs btn-secondary btn-sm">View Profile</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ route('logout') }}">Logout</a>
                                    </li>
                                </div>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- End Navbar -->
        </div>

        <!-- Sidebar -->
        <div class="sidebar sidebar-style-2">           
            <div class="sidebar-wrapper scrollbar scrollbar-inner">
                <div class="sidebar-content text-white main-folder">
                    <!-- <ul class="nav">
                        <li class="nav-item show-actions">
                            <a data-toggle="collapse" href="#appendsubFolder_0" class="collapsed" aria-expanded="false">
                                <i class="icon-folder-alt text-white"></i>
                                <p>My Research Folder</p>
                                <span class="actions" style="display: none;">
                                    <span id="editFolder"><i class="icon-pencil text-white"></i></span>
                                    <span id="shareOther"><i class="icon-user-follow text-white"></i></span>
                                    <span class="addsubFolder addsubFolder_0" id="addsubFolder_0" data-child=0 data-parent=0><i class="icon-plus text-white"></i></span>
                                </span>
                            </a>

                            <div class="collapse" id="appendsubFolder_0">

                            </div>
                        </li>
                    </ul> -->
                    <input type="hidden" id="authId" name="auth-id" value="{{ $user->id }}">
                    <div id="appendFolder">
                        <div class="appendFolder_0">
                        </div>
                    </div>

                    <ul class="nav" id="inputaddFolder">
                        <li>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" class="addFolder" id="addFolder" style="background: none;padding: 0px 5px 0px 0px; border: none;">
                                            <i class="icon-plus p-2 text-white"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="title">
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Sidebar -->

        <div class="main-panel">
            <div class="content">
                @yield('content')       
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <!-- <nav class="pull-left">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link text-white" href="http://www.themekita.com">
                                    ThemeKita
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="#">
                                    Help
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-white" href="#">
                                    Licenses
                                </a>
                            </li>
                        </ul>
                    </nav> -->
                    <div class="copyright ml-auto">
                        &copy; {{ date('Y') }}, Highlight
                    </div>              
                </div>
            </footer>

            <!-- edit Modal -->
            <div class="modal fade" id="editFolder" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header no-bd">
                            <h5 class="modal-title">
                                <span class="fw-mediumbold">
                                New</span> 
                                <span class="fw-light">
                                    Row
                                </span>
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p class="small">Create a new row using this form, make sure you fill them all</p>
                            <form>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Name</label>
                                            <input id="addName" type="text" class="form-control" placeholder="fill name">
                                        </div>
                                    </div>
                                    <div class="col-md-6 pr-0">
                                        <div class="form-group form-group-default">
                                            <label>Position</label>
                                            <input id="addPosition" type="text" class="form-control" placeholder="fill position">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-default">
                                            <label>Office</label>
                                            <input id="addOffice" type="text" class="form-control" placeholder="fill office">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer no-bd">
                            <button type="button" id="addRowButton" class="btn btn-primary">Add</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @include('includes.modal')

    <script src="{{ asset('js/app.js') }}" ></script>

    <!-- jQuery UI -->
    <script src="{{ asset('admin/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="{{ asset('admin/assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/plugin/jquery.magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    
    <!-- jQuery Scrollbar -->
    <script src="{{ asset('admin/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
    <!-- Bootstrap Notify -->
    <script src="{{ asset('admin/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

    <!-- Emoji -->
    <script src="//twemoji.maxcdn.com/2/twemoji.min.js?12.0.0"></script>

    <!-- Sweet Alert -->
    <script src="{{ asset('admin/assets/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Atlantis JS -->
    <script src="{{ asset('admin/assets/js/atlantis.js') }}"></script>
    <!-- Data Table JS -->
    <script src="{{ asset('common/vendor/datatable/datatables.min.js') }}"></script>
    
    <!-- X Editable JS -->
    <script src="{{ asset('common/vendor/x-editable/js/bootstrap-editable.min.js') }}"></script>

    <!-- Select 2 -->
    <script src="{{ asset('common/vendor/select2/js/select2.full.min.js') }}"></script>
    
    <!-- Daterange Picker -->
    <script src="{{ asset('common/vendor/daterangepicker/daterangepicker.js') }}"></script>

    <!-- Image Picker -->
    <script src="{{ asset('common/vendor/fileinput/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('common/vendor/fileinput/js/fileinput.js') }}"></script>
    
    <!-- Validate -->
    <script src="{{ asset('common/vendor/validate/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('common/vendor/validate/additional-methods.js') }}"></script>

    <script src="{{ asset('common/js/script.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
    
    <script>
        var dateFormat = "{{ config("site.date_format.front") }}";
        var dateTimeFormat = "{{ config("site.date_time_format.front") }}";
        var notify = "";
        //Initialize Emoji
        $('body').bind("DOMSubtreeModified",function(){
            twemoji.parse(document.body);
        });
        $(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $( document ).ajaxError(function( event, request, settings ) {
                notifyWarning("Something went wrong");
            });
        });
    </script>

    <script type="text/javascript"> 
        $(document).ready(function(){
            var sibilings = 0;
            $(".main-folder, .mainfolder").on('click', '#addFolder', function ()
            {
                $.ajax({
                    type: "POST",
                    url: "{{ route('users.main-folder') }}",
                    data: {name:'untitled',user_id:$('#authId').val()},
                    dataType: "json",
                    success: function(data) {
                        if(data.success == 1){
                            record = data.data;
                            console.log(data.data);
                            $.each(record, function(index, item){
                                 console.log(item.id);
                                $("<div />", {
                                    class : "appendFolder_"+ item.id,
                                    id: "appendFolder_"+ item.id,
                                    html: '<ul class="nav" id="parentFolder_'+item.id+'"><li class="nav-item show-actions"><a data-toggle="collapse" href="#appendsubFolder_'+item.id+'"><i class="icon-folder-alt text-white"></i><p class="fname">'+item.name+'</p><span class="actions" style="display:none;"><span class="editFolder" data-folder="'+item.id+'"><i class="icon-pencil text-white"></i></span><span id="shareOther"><i class="icon-user-follow text-white"></i></span><span class="addsubFolder addsubFolder_'+item.id+'" data-child=0 data-parent='+item.id+' data-sibilings=0 id="addsubFolder_'+item.id+'"><i class="icon-plus text-white"></i></span></span></a><div class="collapse" id="appendsubFolder_'+item.id+'"></div></li></ul>',
                                    appendTo: "#appendFolder"
                                });  
                            });
                            notifySuccess(data.message);
                        }else{
                            notifyWarning(data.message);
                        }
                    }
                });            
            });
        });

        function loadFolder(){
            $.ajax({
                type: "POST",
                url: "{{route('users.get-mainfolder')}}",
                data: {user_id:$('#authId').val()},
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    if(response.success == 1){
                        record = response.data;
                        $('#appendFolder').empty();
                        $.each(record, function(index, item){
                            $("<div />", {
                                class : "appendFolder_"+ item.id,
                                id: "appendFolder_"+ item.id,
                                html: '<ul class="nav" id="parentFolder_'+item.id+'"><li class="nav-item show-actions"><a data-toggle="collapse" href="#appendsubFolder_'+item.id+'"><i class="icon-folder-alt text-white"></i><p class="fname">'+item.name+'</p><span class="actions" style="display:none;"><span class="editFolder" data-folder="'+item.id+'"><i class="icon-pencil text-white"></i></span><span id="shareOther"><i class="icon-user-follow text-white"></i></span><span class="addsubFolder addsubFolder_'+item.id+'" data-child=0 data-parent='+item.id+' data-sibilings=0 id="addsubFolder_'+item.id+'"><i class="icon-plus text-white"></i></span></span></a><div class="collapse" id="appendsubFolder_'+item.id+'"></div></li></ul>',
                                appendTo: "#appendFolder"
                            });  
                        });
                        //notifySuccess(response.message);
                    }
                    else
                    {
                        //notifyWarning(response.message);
                    }
                }
            });
        }    

        $(window).on('load',function(){
            loadFolder();
        });
    </script>

    <script type="text/javascript">
        /*$(document).ready(function () {
            var parent = 1;
            var sibilings = 0;
            $(".main-folder, .mainfolder").on('click', '#addFolder', function ()
            {   
                $("<div />", {
                    class : "appendFolder_"+ parent,
                    id: "appendFolder_"+ parent,
                    html: '<ul class="nav" id="parentFolder_'+parent+'"><li class="nav-item show-actions"><a data-toggle="collapse" href="#appendsubFolder_'+parent+'"><i class="icon-folder-alt text-white"></i><p>Main</p><span class="actions" style="display:none;"><span id="editFolder"><i class="icon-pencil text-white"></i></span><span id="shareOther"><i class="icon-user-follow text-white"></i></span><span class="addsubFolder addsubFolder_'+parent+'" data-child=0 data-parent='+parent+' data-sibilings=0 id="addsubFolder_'+parent+'"><i class="icon-plus text-white"></i></span></span></a><div class="collapse" id="appendsubFolder_'+parent+'"></div></li></ul>',
                    appendTo: "#appendFolder"
                });  
                parent++;                       
            });

            $(".main-folder, .mainfolder").on('click','.addsubFolder', function ()
            {   
                var divSubFolder = '';
                var data = $(this).attr("data-child");
                var parentId = $(this).attr("data-parent");
                console.log(parentId,data);
                data++;
                divSubFolder = `<ul class="nav nav-collapse childFolder" id="childFolder_${data}"><li class="nav-item show-actions"><a data-toggle="collapse" href="#appendsubFolder_${data}"><i class="icon-folder-alt text-white"></i><p>child</p><span class="actions" style="display:none;"><span id="editFolder"><i class="icon-pencil text-white"></i></span><span id="shareOther"><i class="icon-user-follow text-white"></i></span><span class="addsiblingFolder addsiblingFolder_${data}" id="addsubFolder_${data}" data-child=${data} data-sibilings=0 data-parent=${parentId}><i class="icon-plus text-white"></i></span></span></a><div class="collapse" id="appendsubFolder_${parentId}"></div></li></ul><div class="siblingFolder_${parentId}${data}"></div>`;
                $('.appendFolder_'+parentId).append(divSubFolder);
                $('.addsubFolder_'+parentId).attr('data-child',data);
                
            });
            $(".main-folder, .mainfolder").on('click','.addsiblingFolder', function ()
            {   
                var divSibilings = '';
                var dataParent = $(this).attr("data-parent");
                var dataChild = $(this).attr("data-child");
                dataSibling = $(this).attr("data-sibilings");
                console.log(dataParent,dataChild,dataSibling);
                if(dataSibling <= 2)
                {
                    dataSibling++;
                    //console.log(dataSibling);
                    divSibilings = `<ul class="nav nav-collapse siblingsFolder" id="siblingsFolder_${dataSibling}"><li class="nav-item show-actions"><a data-toggle="collapse" href="#appendsubFolder_${dataChild}"><i class="icon-folder-alt text-white"></i><p>Sibling</p><span class="actions" style="display:none;"><span id="editFolder"><i class="icon-pencil text-white"></i></span><span id="shareOther"><i class="icon-user-follow text-white"></i></span><span class="siblingsFolder" id="addsiblingsFolder_${dataSibling}" data-child=${dataChild} data-sibilings=${dataSibling} data-parent=${dataParent}></span></span></a><div class="collapse" id="appendsubFolder_${dataChild}"></div></li></ul>`;
                    $('.siblingFolder_'+dataParent+dataChild).append(divSibilings);
                    $('.addsiblingFolder_'+dataChild).attr('data-sibilings',dataSibling);
                }
            });
        });*/
    </script>
    
    @stack("js")

    @stack("include_js")

    @include('web.modals.edit-folder')
    
</body>
</html>

{{-- asset('admin/assets/img/profile.jpg') --}}