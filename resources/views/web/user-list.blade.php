@extends("web.layouts.app")

@section("content")
<div class="page-inner">
	<div class="page-header">
		<!-- <h4 class="page-title"></h4>
		<ul class="breadcrumbs">
			<li class="nav-home">
				<a href="#">
					<i class="flaticon-home"></i>
				</a>
			</li>
			<li class="separator">
				<i class="flaticon-right-arrow"></i>
			</li>
			<li class="nav-item">
				<a href="{{ route('private.dashboard') }}">Dashboard</a>
			</li>
			<li class="separator">
				<i class="flaticon-right-arrow"></i>
			</li>
			<li class="nav-item">
				<a href="#"></a>
			</li>
		</ul> -->
	</div>
	<div class="row" id="main-tabs">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<div class="accordion" id="myCollapsible">
						<div class="panel-body">
							@foreach($list as $key => $item)
							<div class="panel-heading">
								<h2 class="mb-0">
									<button class="btn w-100 text-left bg-transparent">
										<div class="d-flex">
											<div class="flex-1 ml-3 pt-1 cols-arrow">
												<h6><a class="page_url" href="javascript:void(0);" data-url="{{ $key }}"
														data-colors="{{ json_encode($item['highlights']) }}">{{ $item['title'] }}</a>
												</h6>
												<span class="text-muted">
													<div class="row gutters-xs">
														@foreach($item['highlights'] as $hl)
														<div class="col-auto">
															<label class="colorinput">
																<input name="color" type="checkbox" value="dark"
																	class="colorinput-input">
																<span class="colorinput-color bg-secondary"
																	style="background-color: {{ $hl['color_class'] }} !important;"></span>
															</label>
														</div>
														@endforeach
														<span>{{ count($item['highlights']) }} Highlighs |
															{{ $item['date']->format('H:i A') }}</span>
													</div>
												</span>
											</div>
										</div>
									</button>
								</h2>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8" id="website">

		</div>
	</div>
</div>
</div>

<!-- edit Modal -->
<div class="modal fade" id="editFolder" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header no-bd">
				<h5 class="modal-title">
					<span class="fw-mediumbold">
						New</span>
					<span class="fw-light">
						Row
					</span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p class="small">Create a new row using this form, make sure you fill them all</p>
				<form>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group form-group-default">
								<label>Name</label>
								<input id="addName" type="text" class="form-control" placeholder="fill name">
							</div>
						</div>
						<div class="col-md-6 pr-0">
							<div class="form-group form-group-default">
								<label>Position</label>
								<input id="addPosition" type="text" class="form-control" placeholder="fill position">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label>Office</label>
								<input id="addOffice" type="text" class="form-control" placeholder="fill office">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer no-bd">
				<button type="button" id="addRowButton" class="btn btn-primary">Add</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


@endsection
@push("css")
<style>
	.export:hover {
		background-color: #1572E8 !important;
		color: #FFF !important;
	}
</style>
@endpush

@push("js")
<script src="{{ asset('js/jquery.mark.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready( function(){
		$("#show-hlights").on('click', function(){
			$(".panel-heading").addClass("active");
			$(".panel-collapse").addClass("show");
		});
		$("#hide-hlights").on('click', function(){
			$(".panel-heading").removeClass("active");
			$(".panel-collapse").removeClass("show");
		});

		$('.panel-collapse').on('show.bs.collapse', function () {
		    $(this).siblings('.panel-heading').addClass('active');
		});

		$('.panel-collapse').on('hide.bs.collapse', function () {
			$(this).siblings('.panel-heading').removeClass('active');
		});
	});

	var colors = "";
	$(document).on('click', '.page_url', function(){
		var url = $(this).data('url');
		colors = $(this).data('colors');

		$('#website').html('');
		$('<iframe>', {
            src: url,
            id: 'website_frame',
            frameborder: 0,
			style:'display:none;width:100%;height:800px',
            onload: function () {
				$(this).show();
                colors.forEach((item, index) => {
					setTimeout(()=>{
						$('body').mark(item.content, { iframes: true, acrossElements: false });
					}, 2000);
				});
            }
        }).appendTo('#website');

		// var ifr=$('<iframe/>', {
		// 	id:'website_frame',
		// 	src:url,
		// 	style:'display:none;width:100%;height:800px',
		// 	load:function(){
		// 		$(this).show();
		// 		alert('iframe loaded !');
		// 	}
		// });
		// $('#website').append(ifr);
	});

</script>
<!-- <script type="text/javascript">
    $(document).ready(function () {
        var parent = 1;
        var sibilings = 0;
        $(".main-folder").on('click', '.addFolder', function ()
        {   
            $("<div />", {
                class : "appendFolder_"+ parent,
                id: "appendFolder_"+ parent,
                html: '<ul class="nav" id="parentFolder_'+parent+'"><li class="nav-item show-actions"><a data-toggle="collapse" href="#appendsubFolder_'+parent+'"><i class="icon-folder-alt text-white"></i><p>Main</p><span class="actions" style="display:none;"><span id="editFolder"><i class="icon-pencil text-white"></i></span><span id="shareOther"><i class="icon-user-follow text-white"></i></span><span class="addsubFolder addsubFolder_'+parent+'" data-child=0 data-parent='+parent+' data-sibilings=0 id="addsubFolder_'+parent+'"><i class="icon-plus text-white"></i></span></span></a><div class="collapse" id="appendsubFolder_'+parent+'"></div></li></ul>',
                appendTo: "#appendFolder"
            });  
            parent++;                       
        });

        $(".main-folder").on('click','.addsubFolder', function ()
        {   
            var divSubFolder = '';
            var data = $(this).attr("data-child");
            var parentId = $(this).attr("data-parent");
            console.log(parentId,data);
            data++;
            divSubFolder = `<ul class="nav nav-collapse childFolder" id="childFolder_${data}"><li class="nav-item show-actions"><a data-toggle="collapse" href="#appendsubFolder_${data}"><i class="icon-folder-alt text-white"></i><p>child</p><span class="actions" style="display:none;"><span id="editFolder"><i class="icon-pencil text-white"></i></span><span id="shareOther"><i class="icon-user-follow text-white"></i></span><span class="addsiblingFolder addsiblingFolder_${data}" id="addsubFolder_${data}" data-child=${data} data-sibilings=0 data-parent=${parentId}><i class="icon-plus text-white"></i></span></span></a><div class="collapse" id="appendsubFolder_${parentId}"></div></li></ul><div class="siblingFolder_${parentId}${data}"></div>`;
            $('.appendFolder_'+parentId).append(divSubFolder);
            $('.addsubFolder_'+parentId).attr('data-child',data);
            
        });
        $(".main-folder").on('click','.addsiblingFolder', function ()
        {   
            var divSibilings = '';
            var dataParent = $(this).attr("data-parent");
            var dataChild = $(this).attr("data-child");
            dataSibling = $(this).attr("data-sibilings");
            console.log(dataParent,dataChild,dataSibling);
            if(dataSibling <= 2)
            {
                dataSibling++;
                //console.log(dataSibling);
                divSibilings = `<ul class="nav nav-collapse siblingsFolder" id="siblingsFolder_${dataSibling}"><li class="nav-item show-actions"><a data-toggle="collapse" href="#appendsubFolder_${dataChild}"><i class="icon-folder-alt text-white"></i><p>Sibling</p><span class="actions" style="display:none;"><span id="editFolder"><i class="icon-pencil text-white"></i></span><span id="shareOther"><i class="icon-user-follow text-white"></i></span><span class="siblingsFolder" id="addsiblingsFolder_${dataSibling}" data-child=${dataChild} data-sibilings=${dataSibling} data-parent=${dataParent}></span></span></a><div class="collapse" id="appendsubFolder_${dataChild}"></div></li></ul>`;
                $('.siblingFolder_'+dataParent+dataChild).append(divSibilings);
                $('.addsiblingFolder_'+dataChild).attr('data-sibilings',dataSibling);
            }
        });
    });
</script> -->
@endpush