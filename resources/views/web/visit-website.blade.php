@extends('web.layouts.plain')
@push('styles')
<style type="text/css">
    .container {
        margin: 100px;
    }

    .selection {
        border-radius: 5px !important;
    }

    #colorOne:focus,
    #colorTwo:focus,
    #colorThree:focus,
    #colorFour:focus,
    #colorFive:focus,
    .btn:focus,
    button:focus {
        outline: none;
    }

    .redcolor {
        background: red;
    }

    #colorOne {
        background-color: rgb(255, 202, 215);
        border-color: rgb(242, 192, 204);
        border-radius: 100px;
        padding: 10px;
    }

    #colorTwo {
        background-color: rgb(255, 222, 112);
        border-color: rgb(242, 211, 106);
        border-radius: 100px;
        padding: 10px;
    }

    #colorThree {
        background-color: rgb(255, 251, 120);
        border-color: rgb(242, 238, 114);
        border-radius: 100px;
        padding: 10px;
    }

    #colorFour {
        background-color: rgb(209, 255, 97);
        border-color: rgb(199, 242, 92);
        border-radius: 100px;
        padding: 10px;
    }

    #colorFive {
        background-color: rgb(180, 255, 235);
        border-color: rgb(171, 242, 223);
        border-radius: 100px;
        padding: 10px;
    }

    .cOne {
        box-shadow: inset 0 -8px 0 #ffcad7;
    }

    .cTwo {
        box-shadow: inset 0 -8px 0 #ffde70;
    }

    .cThree {
        box-shadow: inset 0 -8px 0 #ffde70;
    }

    .cFour {
        box-shadow: inset 0 -8px 0 #ffde70;
    }

    .cFive {
        box-shadow: inset 0 -8px 0 #ffde70;
    }
</style>
@endpush
@section('content')
<div class="row mt-2">
    <div class="col-md-10">
        <div class="form-group">
            <input type="url" name="url" id="url" class="form-control" placeholder="Enter the URL" />
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <button class="btn btn-primary btn-block">Go</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {{-- <div id="hlight">
            To achieve this using iFrame, you must be able to apply jQuery to the external web content that is being
            retrieved via iFrame.

            This solution works same as iFrame. I have created a PHP script that can get all the contents from the other
            website, and most important part is you can easily apply your custom jQuery to that external content. Please
            refer to the following script that can get all the contents from the other website and then you can apply
            your cusom jQuery/JS as well. This content can be used anywhere, inside any element or any page.
        </div> --}}
        <iframe id="myframe" src="https://en.wikipedia.org/wiki/India" frameborder="0" style="height:1024px;width:100%"
            height="100%" width="100%" pointer-events="auto"></iframe>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/selection.js') }}"></script>
<script type="text/javascript">
    var selection = new Selection();
	selection.config({
		facebook: false,
		twitter: false,
		search:false,
		copy:false,
		speak:false,
		translate:false,
		colorOne: true,
		colorTwo: true,
		colorThree: true,
		colorFour: true,
		colorFive: true,
		backgroundColor: '#fff',
		iconColor: '#000',
	}).init();

	// function coneFunction(){
    //     var userSelection = window.getSelection();
	// 	//Attempting to highlight multiple selections (for multiple nodes only + Currently removes the formatting)
	// 	for(var i = 0; i < userSelection.rangeCount; i++) {
	// 		//Copy the selection onto a new element and highlight it
	// 		var node = highlightcOneRange(userSelection.getRangeAt(i)/*.toString()*/);
	// 		// Make the range into a variable so we can replace it
	// 		var range = userSelection.getRangeAt(i);
	// 		//range.getClientRects();
	// 		console.log(range);
	// 		//Delete the current selection
	// 		range.deleteContents();
	// 		//Insert the copy
	// 		range.insertNode(node);
	// 	}
	// }

	// //Function that highlights a selection and makes it clickable
	// function highlightcOneRange(range) {
	//     //Create the new Node
	//     var newNode = document.createElement("span");
	//     // Make it highlight
	//     newNode.setAttribute("class", "cOne");
	//     //console.log(newNode);

	//     //newNode.innerHTML += range;
	//     newNode.appendChild(range.cloneContents());

	//     //range.surroundContents(newNode);
	//     return newNode;
	// }

	// function ctwoFunction(){
    //     var userSelection = window.getSelection();
	// 	//Attempting to highlight multiple selections (for multiple nodes only + Currently removes the formatting)
	// 	for(var i = 0; i < userSelection.rangeCount; i++) {
	// 		//Copy the selection onto a new element and highlight it
	// 		var node = highlightcTwoRange(userSelection.getRangeAt(i)/*.toString()*/);
	// 		// Make the range into a variable so we can replace it
	// 		var range = userSelection.getRangeAt(i);
	// 		range.getClientRects();
	// 		console.log(range);
	// 		//Delete the current selection
	// 		range.deleteContents();
	// 		//Insert the copy
	// 		range.insertNode(node);
	// 	}
	// }

	// //Function that highlights a selection and makes it clickable
	// function highlightcTwoRange(range) {
	//     //Create the new Node
	//     var newNode = document.createElement("span");
	//     // Make it highlight
	//     newNode.setAttribute("class", "cTwo");
	//     //console.log(newNode);

	//     //newNode.innerHTML += range;
	//     newNode.appendChild(range.cloneContents());

	//     //range.surroundContents(newNode);
	//     return newNode;
	// }


</script>
@endpush
