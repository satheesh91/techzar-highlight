<div class="modal fade" id="edit-folder-modal" tabindex="-1" role="dialog" aria-labelledby="editFolder"
  aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <form action="" id="edit-folder-form">
        @csrf
        @method('PUT')
        <input type="hidden" name="folderID" value="" />
        <div class="modal-header">
          <h5 class="modal-title" id="editFolderName">Edit Folder</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="text" class="form-control" name="folderName" />
          </div>
          @php
          $colors = ['#FFCAD7', '#FFDE70', '#FFFB78', '#B4FFEB', '#F8CEFF'];
          @endphp
          @foreach ($colors as $key => $item)
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="color" name="color[{{ $key }}]" value="{{ $item }}" />
                </div>
              </div>
              <input type="text" class="form-control" name="categoryName[{{ $key }}]"
                placeholder="Enter Category Name" />
            </div>
          </div>
          @endforeach
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary btn-sm">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  function getFolder(id){
        $.ajax({
          url: "{{ url('/get-folder') }}/"+id,
          dataType: 'json',
          success: function(res){
            $('#edit-folder-modal form')[0].reset();
            $('#edit-folder-modal [name=folderName]').val(res.folder.name);

            $('#edit-folder-modal [name="color[0]"]').val(res.colors[0].color_code);
            $('#edit-folder-modal [name="categoryName[0]"]').val(res.colors[0].category_name);

            $('#edit-folder-modal [name="color[1]"]').val(res.colors[1].color_code);
            $('#edit-folder-modal [name="categoryName[1]"]').val(res.colors[1].category_name);

            $('#edit-folder-modal [name="color[2]"]').val(res.colors[2].color_code);
            $('#edit-folder-modal [name="categoryName[2]"]').val(res.colors[2].category_name);

            $('#edit-folder-modal [name="color[3]"]').val(res.colors[3].color_code);
            $('#edit-folder-modal [name="categoryName[3]"]').val(res.colors[3].category_name);

            $('#edit-folder-modal [name="color[4]"]').val(res.colors[4].color_code);
            $('#edit-folder-modal [name="categoryName[4]"]').val(res.colors[4].category_name);

          },
          error: function(err){
            alert(res.responseText);
          }
        });
      }

      $(document).on('click', '.editFolder', function(){
        var btn = $(this);
        var folderName = btn.closest('li').find('.fname').html();
        var folderID = btn.data('folder');
        $('#edit-folder-modal [name=folderID]').val(folderID);
        $('#edit-folder-modal [name=folderName]').val(folderName);
        getFolder(folderID);
        $('#edit-folder-modal').modal('show');
      });

      $(document).on('submit', '#edit-folder-form', function(){
        var form = $('#edit-folder-form');
        var ID = form.find('[name=folderID]').val();
        $.ajax({
          url: "{{ url('/update-mainfolder') }}/"+ ID,
          type: "POST",
          data: form.serialize(),
          success: function(res){
            notifySuccess('Folder changes saved');
            $('#edit-folder-modal').modal('hide');
            loadFolder();
          },
          error: function(err){
            notifyWarning(err.responseText);
          }
        });
        return false;
      });
</script>