@extends('admin.layouts.guest')

@section('content')

<div class="wrapper wrapper-login">
    <div class="container container-login animated fadeIn pt-4 pb-4">
        <div id="all-frm">  
            <div class="form-head">
                <h5 class="card-title text-center m-auto" style="max-width: 50px;">
                    <img class="w-100" src="{{ asset('common/img/logo/highlight-logo.png') }}">
                </h5>
                <h5 class="card-title text-center mt-3" id="exlog-txt">
                    Sign up to create your first highlights
                </h5>
                <h5 class="card-title text-center mt-3" id="new-txt">
                    Sign in to access your highlights
                </h5>
                <button class="btn btn-round btn-google btn-block text-uppercase mb-3">
                    <i class="fab fa-google mr-2"></i>
                    Sign in with Google
                </button>
                <button class="btn btn-round btn-facebook btn-block text-uppercase mb-3">
                    <i class="fab fa-facebook-f mr-2"></i>
                    Sign in with Facebook
                </button>
                <button class="btn btn-round btn-block text-uppercase login-skip">
                    Skip this step
                </button>
            </div>

            <div class="mt-3 text-center no-acc">
                Don't have Google or Facebook? <br>
                <span id="signin-frm"> Sign Up with Email</span>
                <div class="mt-3 text-center">
                    <span id="login-frm">Existing user?</span>
                </div>
            </div>

            <div id="login_tab">
                <h5 class="line text-center mt-3 mb-3">
                    <span>OR</span>
                </h5>  
                <form class="login-form form" method="POST" action="{{ route('login') }}" id="loginForm">
                    @csrf
                    <div class="alert alert-warning alert-dismissible fade show text-center  @if(!$errors->any()) d-none @endif" role="alert">
                      <strong> {{__("site.invalid_login") }} </strong>
                    </div>

                    <div class="form-group form-floating-label">
                        <input id="loginEmail" name="email" type="text" class="form-control floating-input input-border-bottom pl-1 pr-1" required value="{{ old('email') }}" autofocus maxlength="{{ limit('email.max')}}">
                        <label for="email" class="placeholder">Email</label>
                    </div>
                    <div class="form-group form-floating-label">
                        <input id="password" name="password" type="password" class="form-control floating-input input-border-bottom pl-1 pr-5" required maxlength="{{ limit('password.max')}}">
                        <label for="password" class="placeholder">Password</label>
                        <div class="show-password">
                            <i class="far fa-eye-slash"></i>
                        </div>
                    </div>
                    <div class="row form-sub m-0">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="remember" class="custom-control-input" id="rememberme" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="rememberme">Remember Me</label>
                        </div>                        
                        <!-- <a href="#" id="show-forgot"  class="link float-right">Forget Password ?</a> -->
                    </div>
                    <div class="form-action mb-3">
                        <button type="submit" class="btn btn-primary btn-rounded btn-login" data-loading-text="Signing In.." data-loading="" data-text="" id="signin">Sign In</button>
                    </div>
                </form>
                <div class="mt-3 text-center">
                    <span id="new-user">New user?</span>
                </div>
                <div class="mt-3 text-center">
                    <span id="show-forgot">Forgot your password?</span>
                </div> 
            </div>

            <div id="signin_tab">
                <h5 class="line text-center mt-3 mb-3">
                    <span>OR</span>
                </h5>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group form-floating-label">
                        <input id="name" type="text" class="form-control floating-input input-border-bottom pl-1 pr-1 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        <label for="name" class="placeholder">Name</label>
                    </div>     

                    <div class="form-group form-floating-label">    
                        <input id="email" type="email" class="form-control floating-input input-border-bottom pl-1 pr-1 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        <label for="email" class="placeholder">E-Mail Address</label>
                    </div>                      

                    <div class="form-group form-floating-label">
                        <input id="password" type="password" class="form-control floating-input input-border-bottom pl-1 pr-1 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        <label for="password" class="placeholder">Password</label>
                    </div>

                    <div class="form-group form-floating-label">
                        <input id="password-confirm" type="password" class="form-control floating-input input-border-bottom pl-1" name="password_confirmation" required autocomplete="new-password">
                        <label for="password" class="placeholder">Confirm Password</label>
                    </div>

                    <div class="form-action mb-3">
                        <button type="submit" class="btn btn-primary btn-rounded btn-login" data-loading-text="Signing In.." data-loading="" data-text="" id="signin">Register</button>
                    </div>                    
                </form>
                
                <div class="mt-3 text-center">
                    <span id="ex-user">Existing user?</span>
                </div>  
            </div>            
        </div>
        <div class="login-account mt-3">
            <span class="msg">&copy; {{ date("Y") }} Highlight</span>            
        </div>
    </div>



    <div id="forgot_tab">       
        <div class="container container-forgot animated fadeIn d-none pt-4 pb-4">
            <h5 class="card-title text-center m-auto" id="exlog-txt" style="max-width: 50px;">
                <img class="w-100" src="{{ asset('common/img/logo/highlight-logo.png') }}">
            </h5>
            <h5 class="card-title text-center" id="exlog-txt">
                Reset your password
            </h5>
            <form class="login-form form" id="forgotPasswordForm">
                <div class="alert alert-warning alert-dismissible fade show text-center d-none" role="alert">
                  <strong></strong>
                </div>
                <div class="form-group form-floating-label">
                    <input  id="forgotEmail" name="email" type="email" class="form-control input-border-bottom floating-input pl-1 pr-1" required maxlength="{{ limit('email.max')}}">
                    <label for="email" class="placeholder">Email</label>
                </div>
                <div class="form-action">
                    <a href="javascript:void(0)" id="show-signin" class="btn btn-danger btn-link btn-login mr-3">Sign In</a>
                    <button type="submit" class="btn btn-primary btn-rounded btn-login" data-loading-text="Checking.." data-loading="" data-text="" id="forgotButton">Send Reset Link</button>
                </div>
                <div class="login-account mt-5">
                    <span class="msg">&copy; {{ date("Y") }} Highlight</span>
                    
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('common/css/login.css') }}">
<style type="text/css">
    #login_tab, #signin_tab, #forgot_tab, #new-txt{
        display: none;
    }
    #new-user, #show-forgot, #signin-frm, #login-frm, #ex-user{
        cursor: pointer;
    }
</style>
@endpush

@push('js')

<script>
   
    $(document).ready(function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                $( document ).ajaxError(function( event, request, settings ) {
                notifyWarning("Something went wrong");
        });

        /**Login Form Validation**/
        $("#loginForm").validate({
            rules: {
                email:  {
                            required: true,
                            email   : true
                        },
                password:{
                            required: true,
                        }
            },
            errorPlacement: function(error, element) {
                if(element.hasClass("select2-hidden-accessible")){
                    error.insertAfter(element.siblings('span.select2'));
                }if(element.hasClass("floating-input")){
                    element.closest('.form-floating-label').addClass("error-cont").append(error);
                    //error.insertAfter();
                }else{
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                form.submit();
                loadButton('#signin');
            }
            
           
        }); 


        /**forgot password Form Validation**/
        $("#forgotPasswordForm").validate({
            rules: {
                email:  {
                            required: true,
                            email   : true
                        }
            },
            errorPlacement: function(error, element) {
                if(element.hasClass("select2-hidden-accessible")){
                    error.insertAfter(element.siblings('span.select2'));
                }if(element.hasClass("floating-input")){
                    element.closest('.form-floating-label').addClass("error-cont").append(error);
                    //error.insertAfter();
                }else{
                    error.insertAfter(element);
                }
            },
            submitHandler: function(form) {
                loadButton('#forgotButton');
                $(form).find(".alert").addClass("d-none");
                var data = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: "{{ route('password.email') }}",
                    data: data,
                    dataType: "json",
                    success: function(data) {
                        loadButton("#forgotButton");
                        $(form).find(".alert").removeClass("d-none").find("strong").html(data.message);
                        if(data.success == 1){
                            notifySuccess(data.message);
                            $(form).find(".alert").removeClass("alert-warning").addClass("alert-success");
                            form.reset();
                        }else{
                            notifyWarning(data.message);
                            $(form).find(".alert").removeClass("alert-success").addClass("alert-warning");
                        }
                    }
                }); 
            }
            
           
        }); 


    });
</script>
<script type="text/javascript">
        $('#signin-frm').on('click', function() {
            $('#signin_tab').fadeToggle("slow");
            $('.login-skip').css('display','none');
            $('.no-acc').css('display','none');
        });
        $('#login-frm').on('click', function() {
            $('#login_tab').fadeToggle("slow");
            $('#signin_tab').css('display','none');
            $('.login-skip').css('display','none');
            $('.no-acc').css('display','none');
            $("#exlog-txt").css('display','none');
            $("#new-txt").css('display','block');
            $('.form-head').css('margin-bottom','30px');
        });
        $('#show-forgot').on('click', function() {
            $('#forgot_tab').fadeToggle("slow");
            $('#all-frm').css('display','none');            
        });
        $('#ex-user').on('click', function() {
            $('#login_tab').fadeToggle("slow");
            $('#signin_tab').css('display','none');
            $('.no-acc').css('display','none');            
        });
        $('#new-user').on('click', function() {
            $('#login_tab').css('display','none');
            $('.form-head').css('margin-bottom','30px');
            $('#signin_tab').css('display','none');
            $('.login-skip').css('display','block');
            $('.no-acc').css('display','block');
            $("#exlog-txt").css('display','block');
            $("#new-txt").css('display','none');
        });
        $('#show-signin').on('click', function() {
            $('#all-frm').css('display','block');
        });
    </script>
@endpush