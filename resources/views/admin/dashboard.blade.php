@extends("admin.layouts.app")

@section('content')
<div class="page-inner">
	{{-- <div class="page-header">
		<h4 class="page-title">{{""}}</h4>
		<ul class="breadcrumbs">
			<li class="nav-home">
				<a href="#">
					<i class="flaticon-home"></i>
				</a>
			</li>
			<li class="separator">
				<i class="flaticon-right-arrow"></i>
			</li>
			<li class="nav-item">
				<a href="{{ route('private.dashboard') }}">Dashboard</a>
			</li>
			<li class="separator">
				<i class="flaticon-right-arrow"></i>
			</li>
			<li class="nav-item">
				<a href="#">{{ "" }}</a>
			</li>
		</ul>
	</div> --}}
	<div class="row">
		<div class="col-md-12">
			<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row pt-2 pb-4">
				<div>
					<h2 class=" pb-2 fw-bold">Dashboard</h2>
					{{-- <h5 class=" op-7 mb-2">Premium Bootstrap 4 Admin Dashboard</h5> --}}
				</div>
			</div>
			<div class="row row-card-no-pd mt--2">
				<div class="col-sm-4 col-md-4">
					<div class="card card-stats card-round">
						<div class="card-body ">
							<div class="row">
								<div class="col-5">
									<div class="icon-big text-center">
										<i class="fas fa-users text-warning"></i>
									</div>
								</div>
								<div class="col-7 col-stats">
									<div class="numbers">
										<a href="{{ route('private.users') }}"><p class="card-category">Users</p></a>
										<h4 class="card-title">{{$user_count}}</h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<div class="card card-stats card-round">
						<div class="card-body">
							<div class="row">
								<div class="col-5">
									<div class="icon-big text-center">
										<i class="fas fa-user-shield text-danger"></i>
									</div>
								</div>
								<div class="col-7 col-stats">
									<div class="numbers">
										 <a href="#"><p class="card-category">Admin Users</p></a>
										<h4 class="card-title">{{$admin_count}}</h4> 
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<div class="card card-stats card-round">
						<div class="card-body">
							<div class="row">
								<div class="col-5">
									<div class="icon-big text-center">
										<i class="fas fa-users text-danger"></i>
									</div>
								</div>
								<div class="col-7 col-stats">
									<div class="numbers">
										 <a href="#"><p class="card-category">Visitor User</p></a>
										<h4 class="card-title">{{-- {{$visitor_count}} --}}</h4> 
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="card full-height">
					<div class="card-body">
						<div class="card-title">Total payment & Payment statistics</div>
						<div class="row p-2 filter-cont mb-4">
						<div class="col-md-3 pl-0">
							<div class="form-group pt-0">
								<label>Status</label>
								<select class="select-filter form-control" data-placeholder="Select a Status" id="status-filter">
	                              	<option value="0">INR</option>
	                              	<option value="1">DOLLER</option>
								</select>
							</div>
						</div>
						<div class="col-md-3 pl-0">
							<div class="form-group pt-0">
								<label>Created Date</label>
								<input type="text" class="form-control bg-white" readonly id="date-filter">
							</div>
						</div>
						<div class="col-md-3 pl-0 " style="display: none;">
							<div class="form-group pt-10 float-right">
								<button class="btn btn-primary btn-border btn-round export">Exports</button>
							</div>
						</div> 
					</div>
							<div class="row" >
									<div class="col-md-3">
										<h6 class="fw-bold text-uppercase text-success op-8 text-center">Total Amount</h6>
										<h3 class="fw-bold total_user text-center"></h3>
									</div>
									<div class="col-md-3">
										<h6 class="fw-bold text-uppercase text-danger op-8 text-center">Today</h6>
										<h3 class="fw-bold today_user text-center"></h3>
									</div>
									<div class="col-md-3">
										<h6 class="fw-bold text-uppercase text-danger op-8 text-center">This Month</h6>
										<h3 class="fw-bold month_user text-center"></h3>
									</div>
									<div class="col-md-3">
										<h6 class="fw-bold text-uppercase text-danger op-8 text-center">This Year</h6>
										<h3 class="fw-bold one_year text-center"></h3>
									</div>

							</div>
							<hr>
							<div class="col-md-12">
								<div id="chart-container">
									<canvas id="totalIncomeChart"></canvas>
								</div>
								<p class="text-center"><b class="chart-year"></b></p>
							</div>
						</div>
					</div>
			</div>

			
		</div>


	</div>
</div>

	
@endsection
@push("css")
<style>
.card-stats a{
text-decoration: none;
}
</style>
@endpush
@push("js")
<script type="text/javascript">
	$(document).ready(function(){

		var startDate = moment().startOf('month').format('YYYY-MM-DD');
		var endDate = moment().format('YYYY-MM-DD');
		var status = $("#status-filter").val(); 
		$('#status-filter').on('change',function(){
			status = $("#status-filter").val();
			chart();
		});
		function chart($this){	
			$.ajax({
					url: "{{ route('private.dashboard.chart') }}",
					type: "POST",
					data: {startDate: startDate,endDate :endDate,Status :status },
					dataType: "json",
					success: function(data){

						$('.total_user').html((data.status == 0)?"Rs:"+data.total_count : "$"+data.total_count );
						$('.month_user').html((data.status == 0)?"Rs:"+data.one_month : "$"+data.one_month );
						$('.today_user').html((data.status == 0)?"Rs:"+data.today_user : "$"+data.today_user);
						$('.chart-year').html(data.chart_year);
						$('.one_year').html((data.status == 0)?"Rs:"+data.one_year : "$"+data.one_year);

						$("canvas#totalIncomeChart").remove();
						$("div#chart-container").append('<canvas id="totalIncomeChart"></canvas>');

						var totalIncomeChart = document.getElementById('totalIncomeChart').getContext('2d');
						//if(window.bar != undefined) 
						//window.bar.destroy(); 
						//window.bar = new Chart(totalIncomeChart, {});
						var mytotalIncomeChart = new Chart(totalIncomeChart, {
						type: 'bar',

						data: {
							labels: data.days,
							datasets : [{
								label: "Rs",
								backgroundColor: '#ff9e27',
								borderColor: 'rgb(23, 125, 255)',
								data: data.day_count,
								//data: [500,400],
							}],
						},

						options: {
							responsive: true,
							maintainAspectRatio: false,
							legend: {
								display: false,
							},
							scales: {
								yAxes: [{
									ticks: {
										display: false //this will remove only the label
									},
									gridLines : {
										drawBorder: false,
										display : false
									}
								}],
								xAxes : [ {
									gridLines : {
										drawBorder: false,
										display : false
									}
								}]
							},
						}
					});		
	   			},

			});

		}

		//DateRange Picker
		$("#date-filter").daterangepicker({
			opens: 'left',
			startDate: moment().startOf('month'),
			endDate: moment(),
			locale: {
				format: '{{ config("site.date_format.front") }}'
			},
			maxDate:moment(),
			ranges: {
				'Today': [moment(), moment()],
				//'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				//'This Month': [moment().startOf('month'), moment().endOf('month')],
				'This Year': [moment().startOf('year'), moment().endOf('year')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			//autoUpdateInput: false,
		}, function(start, end, label) {
			startDate = start.format('YYYY-MM-DD');
			endDate =  end.format('YYYY-MM-DD');
			chart();
		});
		chart().trigger('click');
	});
</script>
@endpush