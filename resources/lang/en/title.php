<?php

return [

	"private" => [
		"login"							=> "Login",
		"user_list"						=> "User List",
		"doner_list"					=> "Doner List",
		"contact_list"					=> "Contact List",
		"transaction"					=> "Transaction List",
		"users"							=> "Dashboard",
		"user_profile"					=> "User Profile",
		"user_post"						=> "User Post",
		"my_profile"					=> "My Profile",
		"adminuser_profile"				=> "Admin User Profile",
		"adminuser_list"				=> "Admin User List",
		"dashboard"						=> "Dashboard",
		"admin_type_list"				=> "User Type",
		"productcategory_list"			=> "Product Category List",
		"productsubcategory_list"		=> "Product Sub Category List",
		"attributecategory_list"		=> "Attribute Category List",
		"attributesubcategory_list"		=> "Attribute Sub Category List",
		"userAttribute_list"			=> "User Attribute Category List",
		"userAttributeSub_list"			=> "User Attribute Sub Category List",
		"question_list"					=> "Question List",
		"product_list"					=> "Product List",
		"product_create"				=> "Product Create",
		"product_edit"					=> "Product Edit",
		"rule_list"						=> "Rule List",
		"rule_create"					=> "Rule Create",
		"rule_edit"						=> "Rule Edit",
		"rule_attribute"				=> "Rule Attribute",
		
	]

];