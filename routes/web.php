<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('web.login');
});
/*Route::prefix("private")->name("private.")->namespace("Admin")->group(function(){
	
	Route::get('/login', 'Auth\LoginController@showLoginForm');
	Route::post('/login-user', 'Auth\LoginController@login')->name('login');
	Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
	Route::post('/store', 'Auth\RegisterController@register')->name('register');
	Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
});*/


Auth::routes();
Route::middleware('auth')->group( function(){
	//User Routes
	Route::get('users',"UserController@index")->name("users");
	Route::post('user/list',"UserController@getList")->name("user.list");
	Route::post('user/count', "UserController@userCount")->name("user.count");
	Route::get('myProfile', "UserController@myProfile")->name("user.profile");
	Route::post('user/status', "UserController@updateStatus")->name("user.status");
	Route::post('user/block', "UserController@updateBlock")->name("user.block");
	Route::post('user/destroy',"UserController@destroy")->name("user.destroy");
	Route::post('profile-update', "UserController@profileUpdate")->name("user.profileUpdate");
	Route::post('password-update', "UserController@passwordUpdate")->name("user.passwordUpdate");

	//Main Folder Route
	Route::post('/add-mainfolder',"MainFolderController@store")->name("users.main-folder");
	Route::post('/get-mainfolder',"MainFolderController@getList")->name("users.get-mainfolder");
	Route::post('/edit-mainfolder',"MainFolderController@edit")->name("users.edit-mainfolder");
	Route::put('/update-mainfolder/{mainFolder}', 'MainFolderController@update')->name('users.update-mainfolder');
	Route::get('/get-folder/{mainFolder}', 'MainFolderController@show')->name('users.show');

	Route::get('/logout', function(){
		auth()->logout();
		return redirect('/')->cookie(cookie()->forget('user_id'));
	});
});

Route::get('admin/login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('/login-user', 'Admin\Auth\LoginController@login')->name('admin.login.user');

Route::prefix("private")->middleware('auth:admin')->name("private.")->namespace("Admin")->group( function(){
	Route::get('dashboard', "DashboardController@index")->name('dashboard');
	Route::get('/home', 'AdminController@index')->name('home');
	//Route::get('/login', 'Auth\LoginController@showLoginForm')->name('form');
	//Route::post('/login-user', 'Auth\LoginController@login')->name('login');
	Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
	Route::post('/store', 'Auth\RegisterController@register')->name('register');
	Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

	/*//Admin User related Routes
	Route::get('adminuser/list', "AdminUserController@index")->name("adminusers");
	Route::post('adminuser/list', "AdminUserController@getList")->name("adminuser.list");
	Route::post('adminuser/create', "AdminUserController@create")->name("adminuser.create");
	Route::post('adminuser/edit', "AdminUserController@edit")->name("adminuser.edit");
	Route::post('adminuser/update', "AdminUserController@update")->name("adminuser.update");
	Route::post('adminuser/status', "AdminUserController@updateStatus")->name("adminuser.status");
	Route::post('adminuser/destroy', "AdminUserController@destroy")->name("adminuser.destroy");
	Route::get('adminuser/profile/{key}', "AdminUserController@viewProfile")->name("adminuser.profile");
	
	Route::post('password-update', "AdminUserController@passwordUpdate")->name("passwordUpdate");
	Route::post('profile-update', "AdminUserController@profileUpdate")->name("profileUpdate");*/
	Route::get('adminuser/list', "AdminUserController@index")->name("adminusers");
    Route::get('my-profile', "AdminUserController@myProfile")->name("myprofile");
    Route::post('password-update', "AdminUserController@passwordUpdate")->name("passwordUpdate");
	Route::post('profile-update', "AdminUserController@profileUpdate")->name("profileUpdate");
	Route::post('dashboard-chart', "AdminUserController@barChart")->name('dashboard.chart');

	//Admin User Type Routes
	Route::get('admin-type', "AdminTypeController@index")->name("admintypes");
	Route::post('admin-type/list', "AdminTypeController@getUserTypeList")->name("admintype.list");
	Route::post('admin-type/create', "AdminTypeController@create")->name("admintype.create");
	Route::post('admin-type/edit', "AdminTypeController@edit")->name("admintype.edit");
	Route::post('admin-type/update', "AdminTypeController@update")->name("admintype.update");
	Route::post('admin-type/status', "AdminTypeController@updateStatus")->name("admintype.status");
	Route::post('admin-type/destroy', "AdminTypeController@destroy")->name("admintype.destroy");


	//Question Routes
	Route::get('question', "QuestionController@index")->name("questions")->middleware("ican:question.view");
	Route::post('question/list', "QuestionController@getList")->name("question.list")->middleware("ican:question.view");
	Route::post('question/create', "QuestionController@create")->name("question.create")->middleware("ican:question.create");
	Route::post('question/edit', "QuestionController@edit")->name("question.edit")->middleware("ican:question.edit");
	Route::post('question/update', "QuestionController@update")->name("question.update")->middleware("ican:question.edit");
	Route::post('question/status', "QuestionController@updateStatus")->name("question.status")->middleware("ican:question.edit");
	Route::post('question/destroy', "QuestionController@destroy")->name("question.destroy")->middleware("ican:question.delete");

	//User Routes
	Route::get('users',"UserController@index")->name("users");
	Route::post('user/list',"UserController@getList")->name("user.list");
	Route::post('user/count', "UserController@userCount")->name("user.count");
	Route::get('user/profile/{key}', "UserController@viewProfile")->name("user.profile");
	Route::post('user/status', "UserController@updateStatus")->name("user.status");
	Route::post('user/block', "UserController@updateBlock")->name("user.block");
	Route::post('user/destroy',"UserController@destroy")->name("user.destroy");

	Route::get('donate',"AdminHomeController@index")->name("donate");
	Route::post('doner/count', "AdminHomeController@donerCount")->name("doner.count");
	Route::post('donate/list',"AdminHomeController@donateList")->name("donate.list");

	Route::get('contact',"AdminHomeController@contact")->name("contact");
	Route::post('contact/count', "AdminHomeController@contactCount")->name("contact.count");
	Route::post('contact/list',"AdminHomeController@contactList")->name("contact.list");
});

Route::get('/dashboard', 'HomeController@index')->name('home');

Route::get('/visit-website', function(){
	return view('web.visit-website');
});