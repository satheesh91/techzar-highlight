<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');


// Route::group(['middleware' => ['auth:api']], function () {
    Route::get('folders/user/{userId}', 'API\FolderController@getByUser')->name('getByUser');
    Route::resource('folders', 'API\FolderController');
    Route::resource('highlights', 'API\HighlightController');
// });

Route::get('/authId', 'API\UserController@getUserId');
Route::post('/highlights/getByURL', 'API\HighlightController@getByURL');

Route::post('/highlight-comments/like', 'API\HighlightCommentController@likeComment');
Route::post('/highlight-comments', 'API\HighlightCommentController@create');

Route::get("/folders/list/user/{userId}", 'API\FolderController@getFoldersList')->name('getListByUser');


