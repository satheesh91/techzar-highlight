<?php

use Illuminate\Database\Seeder;
use App\MainFolder;
class UserMainFolderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		MainFolder::insert([
    		'id' 				=> 1,
            'name' 				=> "My Research Folder",
            'user_id'           => 0,            
    	]);
    }
}
