<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FolderColor extends Model
{
    protected $guarded = ['id'];
}
