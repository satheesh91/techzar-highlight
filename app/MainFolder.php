<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainFolder extends Model
{
    protected $guarded = ['id'];

    public function colors()
    {
        return $this->hasMany('App\FolderColor', 'folder_id', 'id')->orderBy('id', 'ASC');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
