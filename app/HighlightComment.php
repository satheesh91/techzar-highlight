<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HighlightComment extends Model
{
    protected $guarded = ['id'];

    public function likes()
    {
        return $this->hasMany('App\UserLikeDislike', 'comment_id', 'id')->where('like', 1);
    }

    public function dislikes()
    {
        return $this->hasMany('App\UserLikeDislike', 'comment_id', 'id')->where('like', 0);
    }
}
