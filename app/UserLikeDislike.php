<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLikeDislike extends Model
{
    protected $guarded = ['id'];
}
