<?php
namespace App\Listeners;

use App\Events\AuthLoginEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Cookie\CookieJar;
use App\User;

class AuthLoginListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuthLoginEvent $event
     *
     * @return void
     */
    public function handle(AuthLoginEvent $event)
    {

        dd($event->user->id);

        // $user = User::find($event->user->id); // find the user associated with this event
        // $user->last_online_date = new \DateTime;
        // $user->save();

        // $cookieJar = new CookieJar();
        // $cookieJar->queue(cookie('user_id', $user->id));
    }
}