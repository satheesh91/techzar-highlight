<?php

namespace App\Http\Controllers;

use App\MainFolder;
use App\FolderColor;
use Illuminate\Http\Request;
use Validator;

class MainFolderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    private $tableName = "main_folders";

    public function index()
    {
        $response_data["title"] = __("title.private.users");
        return view("web.user-list")->with($response_data);
    }

    public function getList(Request $request)
    {
        $mainFolder = $request->all();
        //dd($mainFolder);
        $validator = Validator::make($mainFolder,
            [
                'user_id'           => 'required',              
            ]);

        if(!$validator->fails()){
            $query = MainFolder::whereIn('user_id', array(0, $request->user_id))->get();
            if($query){                
                $response_data = ["success" => 1, "data"=> $query, "message" => __("validation.create_success",['attr'=>"Main Folder"])];
            }else{
                $response_data = ["success" => 0, "message" => __("site.server_error")];
            }

        }else{
            $response_data = ["success" => 0, "message" => __("validation.check_fields"), "errors" => $validator->errors()];
        }
        return response()->json($response_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mainFolder = $request->all();
        //dd($mainFolder);
        $validator = Validator::make($mainFolder,
            [
                'name'              => 'required|string',
                'user_id'           => 'required|numeric',              
            ]);

        if(!$validator->fails()){
       
            $mainFolder = new MainFolder();
            $mainFolder->name           = $request->name;
            $mainFolder->user_id        = $request->user_id;

            if($mainFolder->save()){
                $data = MainFolder::whereId($mainFolder->id)->get();
                $response_data = ["success" => 1, "data"=> $data, "message" => __("validation.create_success",['attr'=>"Main Folder"])];
                
            }else{
                $response_data = ["success" => 0, "message" => __("site.server_error")];
            }
            
        }else{
            $response_data = ["success" => 0, "message" => __("validation.check_fields"), "errors" => $validator->errors()];
        }
    
        return response()->json($response_data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MainFolder  $mainFolder
     * @return \Illuminate\Http\Response
     */
    public function show(MainFolder $mainFolder)
    {
        return response()->json(['folder' => $mainFolder, 'colors' => $mainFolder->colors], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MainFolder  $mainFolder
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, MainFolder $mainFolder)
    {
        $validator = Validator::make($request->all(),
            [
              'data' => 'required|exists:main_folders,id,deleted_at,NULL',
            ]);
               
        if (!$validator->fails()) 
        { 
            $MainFolderId = $request->data;
            $main_folders = MainFolder::where('id', $MainFolderId)->first();
            if($main_folders)
            {
                $response_data = ["success" => 1, "data" => $main_folders];
            }
            else
            {
                $response_data = ["success" => 0, "message" => __("site.server_error")];
            }
        }
        else
        {
            $response_data = ["success" => 0, "message" => __("validation.check_fields"), "errors" => $validator->errors()];
        }
        return response()->json($response_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MainFolder  $mainFolder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainFolder $mainFolder)
    {
        //FolderColor::where('folder_id', $mainFolder->id)->delete();

        $mainFolder->update(['name' => $request->folderName]);

        if(count($mainFolder->colors)){
            foreach($mainFolder->colors as $key => $color){
                $id = $color->id;
                $rec = FolderColor::find($id);
                $rec->update(['color_code' => $request->color[$key], 'category_name' => $request->categoryName[$key]]);
            }
        } else {
            $colors = $request->color;
            $categoryName = $request->categoryName;
            foreach($colors as $key => $color){
                $folderColor = FolderColor::create([
                    'folder_id' => $mainFolder->id,
                    'color_code' => $color,
                    'category_name' => $categoryName[$key]
                ]);
            }
        }

        return response()->json(1);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MainFolder  $mainFolder
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainFolder $mainFolder)
    {
        //
    }
}
