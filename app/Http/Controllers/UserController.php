<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use Yajra\Datatables\Datatables;
use App\User;
use App\Highlight;
use App\FolderColor;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Encryption\DecryptException;

class UserController extends Controller
{

    public function index()
    {
        $title = __("title.private.users");
        $folder_colors = FolderColor::where('folder_id', 1)->get();
        $highlights = Highlight::where('folder_id', 1)->get();

        $list = [];
        foreach($highlights as $item){
            $count = isset($list[$item->page_url]) ? $list[$item->page_url]['count'] + 1 : 1;
            if(!isset($list[$item->page_url]))
                $list[$item->page_url] = ['title' => $item->page_title, 'date' => $item->created_at, 'count' => $count];
            $list[$item->page_url]['highlights'][] = ['color_class' => $item->color->color_code, 'content' => $item->content];
        }

        // dd($list);

        return view("web.user-list", compact('title', 'list', 'folder_colors'));
    }

    public function userCount()
    {
        $user= Auth::user();
        $response_data = Donate::whereUserId($user->id)->count();
        return response()->json($response_data);
    }
    public function myProfile()
    {
        $response_data["title"] = __("title.private.my_profile");
        $response_data["profile"] = Auth::user();
        return view("web.user.myprofile")->with($response_data);
    }

     public function profileUpdate(Request $request)
    {
        $user = auth()->user();
        $validator = Validator::make($request->all(),
            [
              'name'        => 'required|min:'.limit("name.min").'|max:'.limit("name.max").'|valid_name|not_exists:admin_users,name,deleted_at,NULL',
              'email'       => 'required|email|min:'.limit("email.min").'|max:'.limit("email.max").'|unique:users,email,'.$user->id.',id,deleted_at,NULL',
              'phone'       => 'required|numeric|digits:'.limit("phone.max").'|unique:users,phone,'.$user->id.',id,deleted_at,NULL',
            ]);
        if(!$validator->fails()){

            $user->name        = $request->name;
            $user->email       = $request->email;
            $user->phone       = $request->phone;
            if($user->save()){
                $response_data = ["success" => 1, "message" => __("validation.update_success", ["attr" => "Profile"])];
            }else{
                $response_data = ["success" => 0, "message" => __("site.server_error")];
            }

        }else{
            $response_data = ["success" => 0, "message" => __("validation.check_fields"), "errors" => $validator->errors()->toArray()];
        }
        return response()->json($response_data);
    }

    public function passwordUpdate(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            "current_password"  => "required",
            'password'          => 'required|confirmed|min:'.limit("password.min").'|max:'.limit("password.max").'|password|different:current_password',
        ]);

        if(!$validator->fails()){
            $user = auth()->user();
            if (Hash::check($request->current_password, $user->password)) {
                $user->password = bcrypt($request->password);
                if($user->save()){
                    $response_data = ["success" => 1, "message" => __("validation.update_success", ["attr" => "Password"])];
                }else{
                    $response_data = ["success" => 0, "message" => __("site.server_error")];
                }
            }else{

                $response_data = ["success" => 0,  "message" => __("site.invalid_password")];
            }
        }else{
            $response_data = ["success" => 0,  "message" => __("validation.check_fields"), "errors" => $validator->errors()->toArray()];
        }

        return response()->json($response_data);

    }

    public function getList(Request $request)
    {
        $query = Donate::select();
        $user  = auth()->user();
        if ($request->filled('start_date')) {
            $query->whereDate('created_at', '>=', "{$request->get('start_date')}");
        }

        if ($request->filled('end_date')) {
            $query->whereDate('created_at', '<=', "{$request->get('end_date')}");
        }
       return Datatables::of($query->whereUserId($user->id)->get())->make(true);
    }
}
