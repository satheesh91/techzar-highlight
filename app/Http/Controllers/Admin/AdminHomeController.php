<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use App\AdminUser;
use App\Donate;
use Avatar;
use App\Contact;

class AdminHomeController extends Controller
{
	public function index()
    {
        $response_data["title"] = __("title.private.doner_list");
        return view("admin.donate")->with($response_data);
    }

    public function donerCount()
    {
        $response_data = Donate::count();
        return response()->json($response_data);
    }

    public function donateList(Request $request)
    {
  
        $query = Donate::select();
        if($request->has("status") && $request->status != ""){
            $query->whereType($request->status);
        }
        if ($request->filled('start_date')) {
            $query->whereDate('created_at', '>=', "{$request->get('start_date')}");
        }

        if ($request->filled('end_date')) {
            $query->whereDate('created_at', '<=', "{$request->get('end_date')}");
        }
       return Datatables::of($query->get())->make(true);
    }


    public function contact()
    {
        $response_data["title"] = __("title.private.contact_list");
        return view("admin.contact")->with($response_data);
    }

    public function contactCount()
    {
        $response_data = Contact::count();
        return response()->json($response_data);
    }

    public function contactList(Request $request)
    {
  
        $query = Contact::select();
        if($request->has("status") && $request->status != ""){
            $query->whereType($request->status);
        }
        if ($request->filled('start_date')) {
            $query->whereDate('created_at', '>=', "{$request->get('start_date')}");
        }

        if ($request->filled('end_date')) {
            $query->whereDate('created_at', '<=', "{$request->get('end_date')}");
        }
       return Datatables::of($query->get())->make(true);
    }
}
