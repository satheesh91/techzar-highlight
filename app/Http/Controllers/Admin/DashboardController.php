<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\AdminUser;
use App\Visitor;
use Auth;
class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::Guard('admin')->user();
        $response_data["title"] = __("title.private.dashboard");
        $response_data['user_count'] = User::count();
        $response_data['admin_count'] =  AdminUser::count();
        // $response_data['visitor_count'] = Visitor::count();
        return view('admin.dashboard')->with($response_data);
    }
}

