<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use App\AdminUser;
use App\Donate;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Avatar;
class AdminUserController extends Controller
{

	public function index()
    {
        $response_data["title"] = __("title.private.adminuser_list");
        $response_data["userTypes"] = AdminUserType::whereAdminType(2)->whereActive(1)->get();
        return view("admin.adminuser.list")->with($response_data);
    }

    public function getList(Request $request)
    {
        $query = AdminUser::whereHas('userType', function ($query) {
                    $query->where('admin_type', '=', 2);
                });
        
        if($request->has("status") && $request->status != ""){
            $query->whereActive($request->status);
        }
        if($request->has("user_type") && $request->user_type != ""){
            $query->whereUserType($request->user_type);
        }
       return Datatables::of($query->get())->make(true);
    }
    public function myProfile(Request $request)
    {
        $response_data["title"] = __("title.private.my_profile");
        $response_data["profile"] = Auth::Guard('admin')->user();;
        return view("admin.user.myprofile")->with($response_data);
    }

    public function passwordUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), 
        [
            "current_password"  => "required",
            'password'          => 'required|confirmed|min:'.limit("password.min").'|max:'.limit("password.max").'|password|different:current_password',
        ]);

        if(!$validator->fails()){
            $user =  Auth::Guard('admin')->user();
            if (Hash::check($request->current_password, $user->password)) {
                $user->password = bcrypt($request->password);
                if($user->save()){
                    $response_data = ["success" => 1, "message" => __("validation.update_success", ["attr" => "Password"])];
                }else{
                    $response_data = ["success" => 0, "message" => __("site.server_error")];
                }
            }else{

                $response_data = ["success" => 0,  "message" => __("site.invalid_password")];
            }
        }else{
            $response_data = ["success" => 0,  "message" => __("validation.check_fields"), "errors" => $validator->errors()->toArray()];
        }

        return response()->json($response_data);
        
    }

    public function profileUpdate(Request $request)
    {
        $user =  Auth::Guard('admin')->user();
        $validator = Validator::make($request->all(),
            [
              'name'        => 'required|min:'.limit("name.min"),
              'email'       => 'required|email|min:'.limit("email.min"),
              'phone'       => 'required|numeric|digits:'.limit("phone.max"),
              'profile_pic' => 'nullable|image|mimes:'.limit("profile_pic.format").'|max:'.limit("profile_pic.max"),
            ]);
        if(!$validator->fails()){

        	$filePath = "images/admin/adminuser/profile/";
            if($request->hasFile('profile_pic')){
                $user->profile_pic = url("storage/".$request->file('profile_pic')->store($filePath));
            }

            $user->name        = $request->name;
            $user->email       = $request->email;
            $user->phone       = $request->phone;
            $user->updated_by  = $user->id;

            if($user->save()){
                $response_data = ["success" => 1, "message" => __("validation.update_success", ["attr" => "Profile"])];
            }else{
                $response_data = ["success" => 0, "message" => __("site.server_error")];
            }
            
        }else{
            $response_data = ["success" => 0, "message" => __("validation.check_fields"), "errors" => $validator->errors()->toArray()];
        }
        return response()->json($response_data);
    }

    

    //Employee Delete
    public function destroy(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
              'data' => 'required|exists:admin_users,id,deleted_at,NULL',
            ]);
            
        if (!$validator->fails()) 
        { 
            $user = AdminUser::find($request->data); 
            if(!$user->isAdmin()){
                if($user->delete()){
                    $response_data =  ['success' => 1, 'message' => __('validation.delete_success',['attr'=>'Employee'])]; 
                }else{
                    
                    $response_data =  ['success' => 1, 'message' => __('site.server_error')]; 
                }    
            }else{
                $response_data =  ['success' => 0, 'message' => __('validation.refresh_page')];
            }

        }else
        {
            $response_data =  ['success' => 0, 'message' => __('validation.refresh_page')];
        }

        return response()->json($response_data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function logout()
    {
    	Auth::logout();
        return redirect(route("login"));
    }

    public function barChart(Request $request)
    {
        $start_date=$request->startDate;
        $end_date=$request->endDate;
        $dates = [];
        $days  = [];
        $response_data = [];
        $currentMonth  = date('M');
        $currentDate   = date('D');
        $currentyear   = date('Y');
        $carbon_Date   = new Carbon($start_date);
        $getMonth      = $carbon_Date->format('M');
        $getYear       = $carbon_Date->format('Y');
        $carbonEndDate = new Carbon($end_date);
        $getEndyear    = $carbonEndDate->format('Y');

        $period = CarbonPeriod::create($start_date, $end_date);
        $i=0;
        foreach ($period as $date)
        {
            $date_count= $i++;
        }

       if($date_count < 32)
       {
            foreach ($period as $date){
                $day = $date->format('Y-m-d');
                if($request->Status == 0)
                {
                    $user_count =  Donate::whereType(0)->whereDate('created_at',$day)->first();

                }else{

                    $user_count =  Donate::whereType(1)->whereDate('created_at',$day)->first();
                }

                if($user_count)
                    {
                        $amount = $user_count->amount;
                    }else
                    {
                        $amount = 0;
                    }
                $dates[]    = $amount;
                $now        = new Carbon($day); 
                $days[]     = $now->format('d-M');
            }
            if($getYear == $getEndyear)
            {   
                $response_data['chart_year']= $getYear;
            }else
            {
                $response_data['chart_year']= $getYear."-".$getEndyear;
            }
       }
       elseif($currentMonth == $getMonth && $getYear == $getEndyear)
       {
            foreach ($period as $date) {
                $day = $date->format('Y-m-d');
                if($request->Status == 0)
                    {
                        $user_count =  Donate::whereType(0)->whereDate('created_at',$day)->first();

                    }else{

                         $user_count =  Donate::whereType(1)->whereDate('created_at',$day)->first();
                    }
                if($user_count)
                    {
                        $amount = $user_count->amount;
                    }else
                    {
                        $amount = 0;
                    }
                $dates[]    = $amount;
                $now        = new Carbon($day); 
                $days[]     = $now->format('d-M');
            }
            if($getYear == $getEndyear)
            {   
                $response_data['chart_year']= $getYear;
            }else
            {
                $response_data['chart_year']= $getYear."-".$getEndyear;
            }
        }elseif($currentMonth != $getMonth && $getYear == $getEndyear)
        {   
            $monthStartDate         = new Carbon($start_date);
            $monthEndDate           = new Carbon($end_date);
            $getstartMonth          = round($monthStartDate->format('m'));
            $getEndMonth            = round($monthEndDate->format('m'));
            $month =[
                '1' => 'Jan',
                '2' => 'Feb',
                '3' => 'Mar',
                '4' => 'Apr',
                '5' => 'May',
                '6' => 'Jun',
                '7' => 'July',
                '8' => 'Aug',
                '9' => 'Sep',
                '10' => 'Oct',
                '11' => 'Nov',
                '12' => 'Dec',
            ];

            for($getstartMonth; $getstartMonth <= $getEndMonth;$getstartMonth++)
            {
                if($request->Status == 0)
                {
                    $response = Donate::whereType(0)->whereRaw('MONTH(created_at) = ?',[$getstartMonth])->selectRaw('sum(amount) as amount' )->first();

                }else{

                    $response = Donate::whereType(1)->whereRaw('MONTH(created_at) = ?',[$getstartMonth])->selectRaw('sum(amount) as amount' )->first();
                }
                
                if($response)
                    {
                        $amount = $response->amount;
                    }else
                    {
                        $amount = 0;
                    }
                $dates[]    = $amount;
                $days[]=$month[$getstartMonth];
            }

            if($getYear == $getEndyear)
            {   
                $response_data['chart_year']= $getYear;
            }else
            {
                $response_data['chart_year']= $getYear."-".$getEndyear;
            }

        }elseif($getYear != $getEndyear){
            $yearStartDate          = new Carbon($start_date);
            $yearEndDate            = new Carbon($end_date);
            $getstartYear           = $yearStartDate->format('Y');
            $getEndYear             = $yearEndDate->format('Y');
            for($getstartYear; $getstartYear <= $getEndYear;$getstartYear++)
            {
                if($request->Status == 0)
                {
                    $response   = Donate::whereType(0)->whereRaw('YEAR(created_at) = ?',[$getstartYear])->selectRaw('sum(amount) as amount' )->first();
                }else
                {
                    $response   = Donate::whereType(1)->whereRaw('YEAR(created_at) = ?',[$getstartYear])->selectRaw('sum(amount) as amount' )->first();
                }
                if($response)
                    {
                        $amount = $response->amount;
                    }else
                    {
                        $amount = 0;
                    }
                $dates[]    = $amount;
                $days[]     = $getstartYear;
            }

            if($getYear == $getEndyear)
            {   
                $response_data['chart_year']= $getYear;
            }else
            {
                $response_data['chart_year']= $getYear."-".$getEndyear;
            }
        }

        $response_data['days']        = $days;
        $response_data['day_count']   = $dates;
        $response_data['status']      = $request->Status;
        
        if($request->Status == 0)
            {
                 $tot = Donate::whereType(0)->selectRaw('sum(amount) as amount' )->first();
                 $tt = (null == $tot->amount) ? '0' :  $tot->amount;
                $response_data['total_count']  = $tt;
                
                $month = Donate::whereType(0)->whereRaw('MONTH(created_at) = ?',[date('m')])->whereRaw('YEAR(created_at) = ?',[date('Y')])->selectRaw('sum(amount) as amount' )->first();
                $m = (null == $month->amount) ? '0' : $month->amount;
                $response_data['one_month']  = $m;


                $today  = Donate::whereType(0)->whereRaw('DAY(created_at) = ?',[date('d')])->whereRaw('MONTH(created_at) = ?',[date('m')])->whereRaw('YEAR(created_at) = ?',[date('Y')])->selectRaw('sum(amount) as amount' )->first();
                $t = (null == $today->amount) ? '0' : $today->amount;
                $response_data['today_user']  = $t;


                $year  = Donate::whereType(0)->whereRaw('YEAR(created_at) = ?',[date('Y')])->selectRaw('sum(amount) as amount' )->first();
                $y = (null == $year->amount) ? '0' : $year->amount;
                $response_data['one_year']  = $y;

            }else{

                $tot = Donate::whereType(1)->selectRaw('sum(amount) as amount' )->first();
                 $tt = (null == $tot->amount) ? '0' :  $tot->amount;
                $response_data['total_count']  = $tt;

                $month = Donate::whereType(1)->whereRaw('MONTH(created_at) = ?',[date('m')])->whereRaw('YEAR(created_at) = ?',[date('Y')])->selectRaw('sum(amount) as amount' )->first();
                $m = (null == $month->amount) ? '0' : $month->amount;
                $response_data['one_month']  = $m;


                $today  = Donate::whereType(1)->whereRaw('DAY(created_at) = ?',[date('d')])->whereRaw('MONTH(created_at) = ?',[date('m')])->whereRaw('YEAR(created_at) = ?',[date('Y')])->selectRaw('sum(amount) as amount' )->first();
                $t = (null == $today->amount) ? '0' : $today->amount;
                $response_data['today_user']  = $t;


                $year  = Donate::whereType(1)->whereRaw('YEAR(created_at) = ?',[date('Y')])->selectRaw('sum(amount) as amount' )->first();
                $y = (null == $year->amount) ? '0' : $year->amount;
                $response_data['one_year']  = $y;

            }
        

        return response()->json($response_data);

    }
}
