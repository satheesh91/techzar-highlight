<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use Validator;
use Avatar;
use Yajra\Datatables\Datatables;
use App\User;
use App\UserProfile;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Contracts\Encryption\DecryptException;

class UserController extends Controller
{
    
    public function index()
    {
        $response_data["title"] = __("title.private.user_list");
        return view("admin.user.list")->with($response_data);
    }

    public function userCount()
    {
        $response_data = User::count();
        return response()->json($response_data);
    }

    //Blocked User
    public function updateBlock(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    "value" => "required|in:0,1",
                    "pk" => "required|exists:users,id",
                ]);
        //dd($request->all());
        if(!$validator->fails()){
            $user = User::find($request->pk);
            if($request->value == 1)
            {
                $user->blocked_at = now();
            }
            elseif($request->value==0)
            {
                $user->blocked_at =NULL;
            }
            
            if($user->save()){
                $response_data = ["success" => 1, "message" => __("validation.update_success", ["attr" => "User Blocked"])];
            }else{
                $response_data = ["success" => 0, "message" => __("site.server_error")];
            }
        }else{
            $response_data = ["success" =>  0, "message" => __("validation.refresh_page")];
        }
       return response()->json($response_data);
    }



    public function getList(Request $request)
    {
        $query = User::select();
        if($request->has("status") && $request->status != ""){
            $query->whereActive($request->status);
        }
        if ($request->filled('start_date')) {
            $query->whereDate('created_at', '>=', "{$request->get('start_date')}");
        }

        if ($request->filled('end_date')) {
            $query->whereDate('created_at', '<=', "{$request->get('end_date')}");
        }
       return Datatables::of($query->get())->make(true);
    }

    //User Delete
    public function destroy(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
              'data' => 'required|exists:users,id,deleted_at,NULL',
            ]);
            
        if (!$validator->fails()) 
        { 
            $user = User::find($request->data);    
            if($user->delete()){
                $response_data =  ['success' => 1, 'message' => __('validation.delete_success',['attr'=>'User'])]; 
            }else{
                
                $response_data =  ['success' => 1, 'message' => __('site.server_error')]; 
            }

        }else
        {
            $response_data =  ['success' => 0, 'message' => __('validation.refresh_page')];
        }

        return response()->json($response_data);
    }

    public function logout()
    {
    	Auth::logout();
        return redirect(route("login"));
    }

}
