<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User; 
use Auth; 
use Validator;
use Illuminate\Support\Facades\Cookie;

class UserController extends Controller
{
    public $successStatus = 200;

    public function login(Request $request){ 
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email', 
            'password' => 'required'
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){ 
            $user = Auth::user(); 
            $result = [
                'token' => $user->createToken('TechZAR')->accessToken,
                'user' => $user
            ];
            return response()->json($result, $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function getUserId(Request $request){
        return response()->json($_COOKIE['user_id']);
    }
}
