<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HighlightComment;
use App\UserLikeDislike;
use Validator;

class HighlightCommentController extends Controller
{
    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'highlight_id' => 'required',
            'comment' => 'required',
            'user_id' => 'required'
        ]);
        if ($validator->fails()) return response()->json(['errors'=>$validator->errors()]);

        $comment = HighlightComment::create($request->all());

        return response()->json($comment, 201);
    }

    public function likeComment(Request $request){
        $validator = Validator::make($request->all(), [
            'comment_id' => 'required',
            'user_id' => 'required'
        ]);
        if ($validator->fails()) return response()->json(['errors'=>$validator->errors()]);

        $result = UserLikeDislike::where('comment_id', $request->comment_id)->where('user_id', $request->user_id);
        if($result->exists()){
            $data = $result->first();
            $data->update(['like' => $request->like]);
        } else {
            $result = UserLikeDislike::create($request->all());
        }

        return response()->json(1);
    }
}
