<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Highlight;
use App\MainFolder;
use App\UserLikeDislike;
use Validator;

class HighlightController extends Controller
{
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'folder_id' => 'required',
            'color_id' => 'required',
            'content' => 'required',
            'page_title' => 'required',
            'page_url' => 'required|url'
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $highlight = Highlight::create($request->all());

        return response()->json($highlight, 201);
    }

    public function getByURL(Request $request){
        $highlights = Highlight::with(['user', 'comments','color','folder', 'comments.likes', 'comments.dislikes'])->where('page_url', $request->url);
        if($request->has('folder')) {
            $folder = MainFolder::where('name', $request->folder)->first();
            $highlights = $highlights->where('folder_id', $folder->id);
        }
        $highlights = $highlights->get();
        return response()->json(['highlights' =>$highlights], 200);
    }

    // public function createLikeDislike(Request $request){
    //     $exists = UserLikeDislike::where('highlight_id', $request->highlight_id)->where('user_id', $request->user_id)->where('like', $request->like)->exists();

    //     if($exists)
    //         return response()->json('Already voted', 400);

    //     $result = UserLikeDislike::where('highlight_id', $request->highlight_id)->where('user_id', $request->user_id)->first();
    //     if($result) $result->update(['like' => $request->like]);
    //     else {
    //         $result = UserLikeDislike::create([
    //             'highlight_id' => $request->highlight_id,
    //             'user_id' => $request->user_id,
    //             'like' => $request->like
    //         ]);
    //     }

    //     $like_count =  UserLikeDislike::where('highlight_id', $request->highlight_id)->where('user_id', $request->user_id)->where('like', 1)->count();
    //     $dislike_count =  UserLikeDislike::where('highlight_id', $request->highlight_id)->where('user_id', $request->user_id)->where('like', 0)->count();
    //     return response()->json(['likes' => $like_count, 'dislikes' => $dislike_count]);
    // }
}
