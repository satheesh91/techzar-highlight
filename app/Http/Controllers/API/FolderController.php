<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MainFolder;

class FolderController extends Controller
{
    public function show($name){
        $name = urldecode($name);
        $folder = MainFolder::where('name', $name)->with('colors')->first();
        return response()->json(['folder' => $folder], 200);
    }

    public function getByUser($userId){
        $folder = MainFolder::with('colors')->where('user_id', $userId)->orderBy('created_at', 'ASC')->first();
        return response()->json(['folder' => $folder], 200);
    }

    public function getFoldersList($userId){
        $folders = MainFolder::with('colors')->where('user_id', $userId)->orderBy('created_at', 'ASC')->get();
        return response()->json(['folders' => $folders], 200);
    }
}
