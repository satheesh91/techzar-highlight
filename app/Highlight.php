<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Highlight extends Model
{
    protected $guarded = ['id'];

    public function folder()
    {
        return $this->belongsTo('App\MainFolder', 'folder_id', 'id');
    }

    public function color()
    {
        return $this->belongsTo('App\FolderColor', 'color_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\HighlightComment', 'highlight_id', 'id');
    }
}
